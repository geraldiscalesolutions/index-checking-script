import pandas as pd
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from urllib.parse import quote

def create_session(engine):
    session = sessionmaker(bind=engine)
    return session()

def create_engine(config):
    return db.create_engine("mysql+pymysql://%s:%s@%s:%s/%s" % (config['username'], quote(config['password']), config['host'], config['port'], config['database']))

def get_db_data(conn, query):
    print("Getting input from database...")
    engine = create_engine(conn)
    df_input = pd.read_sql_query(query, con=engine)
    engine.dispose()
    print("Has DB Data: %s" % (not df_input.empty))

    return df_input