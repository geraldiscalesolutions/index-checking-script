import pandas as pd
from elasticsearch import Elasticsearch, helpers
import constant

def get_elastic_client():
    return Elasticsearch(
        constant.es_server,
        http_compress=True,
        timeout=60)

def get_es_data(esclient, index_name, query):
    print("Getting data from elasticsearch...")
    result = helpers.scan(client=esclient, query=query, index=index_name, preserve_order=True)
    list_result = list(result)
    df_result = pd.DataFrame(list_result)

    if not df_result.empty and list_result[0]['_source']:
        return list_result[0]['_source']

    return None