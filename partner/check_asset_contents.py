import partner, input

def check_assets(asset):
    asset_ctr = 0
    diff_name = []
    not_found = {}
    for _ind in not_found_index:
        not_found[_ind] = []

    for index,row_ind in enumerate(row_index):
        row_ind = row_ind+" Asset"
        if row_ind in asset:
            check_name = asset[creator_index] if creator_index in asset and index == 0 else None
            asset_info = partner.check_asset(url = asset[row_ind], check_name=check_name)

            if asset_info is not None:
                asset_ctr += 1

                if not asset_info["is_found"]:
                    not_found["all"].append({
                        "url" : asset_info["url"]
                    })

                    if asset_info["not_found_at"] == "db":
                        not_found["db"].append({
                            "url" : asset_info["url"]
                        })
                    else:
                        not_found[asset_info["not_found_at"]].append({
                            "url" : asset_info["url"],
                            "partner_id" : asset_info["partner_id"],
                            "asset_id" : asset_info["asset_id"],
                        })

                if asset_info["has_details"] and "diff_name" in asset_info:
                    diff_name.append({
                        "partner_id" : asset_info["partner_id"],
                        "partner_id_ref" : asset_info["partner_id_ref"],
                        "name_at_excel" : asset[creator_index],
                        "name_at_db" : asset_info["diff_name"]["db"],
                        "name_at_index" : asset_info["diff_name"]["index"]
                    })

    return {
        "ctr" : asset_ctr,
        "not_found" : not_found,
        "diff_name" : diff_name
    }

def check_contents(content):
    not_found = {}
    for _ind in not_found_index:
        not_found[_ind] = []

    for row_ind in row_index:
        row_ind = row_ind+" Post"
        if row_ind in content:
            content_info = partner.check_content(content[row_ind])

            if content_info is not None:
                # content_info["raw_url"] = partner.main.SocialAsset.generate_url_id(content[row_ind])
                not_found_at = content_info["at"]
                content_info.pop("at")
                not_found["all"].append(content_info)
                not_found[not_found_at].append(content_info)

    return {"not_found" : not_found}

def print_stat(asset = [], content = []):
    print("Total Assets Checked: %s" % (asset["checked_ctr"]))

    for _ind in not_found_index:
        if _ind != "all":
            print("Total Assets Missing at %s: %s" % (_ind, len(asset["not_found"][_ind])))

    # print("Total Contents Checked: %s" % (content["checked_ctr"]))

    for _ind in not_found_index:
        if _ind != "all" and _ind != "db":
            print("Total Contents Missing at %s: %s" % (_ind, len(content["not_found"][_ind])))

    print()

def process():
    esclient = partner.main.es.get_elastic_client()
    partner.set_esclient(esclient)
    partner.set_conn(conn)
    partner.set_market_id(market_id)

    asset_checked_ctr = 0
    diff_name = []
    asset_not_found = {}
    content_not_found = {}
    for _ind in not_found_index:
        asset_not_found[_ind] = []
        content_not_found[_ind] = []

    partner.main.init_xls_file()
    pd_input = partner.main.get_input_file(root_folder, file_name, "xlsx")
    pd_input_json = pd_input.to_dict(orient="records")

    for index,row in enumerate(pd_input_json):
        print("=====================")
        print("Checking [%s]: Row %s of %s." % (market_id, str(index+1), str(len(pd_input_json)) ))

        assets = check_assets(row)
        contents = check_contents(row)

        print()
        print("Diff Name: %s" % (len(assets["diff_name"]) > 0))
        print("Missing Asset: %s" % (len(assets["not_found"]["all"])))
        print("Missing Content: %s" % (len(contents["not_found"]["all"])))
        print()

        asset_checked_ctr += assets["ctr"]
        diff_name += assets["diff_name"]
        for _ind in not_found_index:
            asset_not_found[_ind] += assets["not_found"][_ind]
            content_not_found[_ind] += contents["not_found"][_ind]

    print_stat(asset = {
        "checked_ctr" : asset_checked_ctr,
        "not_found" : asset_not_found
    }, content= {
        "not_found" : content_not_found

    })

    partner.print_asset_results(root_folder = root_folder, market = market_id, file_name = file_name, diff_name = diff_name, not_found=asset_not_found)
    partner.print_content_results(root_folder = root_folder, market = market_id, file_name = file_name, not_found=content_not_found)

if __name__ == "__main__":
    conn = partner.main.constant.db_conn["main"]
    market_id = input.market_id
    file_name = input.file_name
    root_folder = "../"

    creator_index = input.creator_index
    row_index = [
        "YT",
        "IG",
        "FB"
    ]

    not_found_index = [
        "db",
        "dc",
        "index",
        "all"
    ]

    process()