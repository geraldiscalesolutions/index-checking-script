import os,sys
import math
import pandas as pd
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from elasticsearch import Elasticsearch, helpers
sys.path.append('../')
import constant


def create_session(engine):
    session = sessionmaker(bind=engine)
    return session()

def create_engine(config):
    return db.create_engine("mysql+pymysql://"+config["user"]+":"+config["password"]+"@"+config["host"]+":"+str(config["port"])+"/"+config["database"])

def get_elastic_client():
    es_client_servers = Elasticsearch(
        constant.es_server,
        http_compress=True,
        timeout=60)

    return es_client_servers

def get_trimmed_url(url):
    url = url.strip('/')
    url = url.strip('?')
    url = url.strip('/')
    url = url.strip(' ')

    return url

def get_es_asset_query(url):
    return {
        "query": {
            "term":{
                "asset.url.display_value": url
            }
        },
        "sort" : {
            "published_date" : "desc"
        },
        "_source" : [
            "published_date"
        ],
        "size" : 1
    }

def get_es_content_query(url):
    return {
        "query": {
            "term":{
                "url.display_value": url
            }
        },
        "sort" : {
            "published_date" : "desc"
        },
        "_source" : [
            "published_date"
        ],
        "size" : 1
    }

def get_result(esclient, url, is_asset = True):
    if is_asset:
        qry = get_es_asset_query(url)
    else:
        qry = get_es_content_query(url)


    result = helpers.scan(client=esclient, query=qry, index=index_name, preserve_order=True)
    list_result = list(result)
    df_result = pd.DataFrame(list_result)

    if not df_result.empty and list_result[0]['_source']:
        return list_result[0]['_source']

    return None

def check(esclient, assets, contents):
    not_found_assets = [];
    not_found_contents = [];

    for url in assets:
        if not (url == '' or (isinstance(url, float) and  math.isnan(url))):
            _url = get_trimmed_url(url)

            result = get_result(esclient, _url)
            if result is None:
                not_found_assets.append({"url" : _url})

    for url in contents:
        if not (url == '' or (isinstance(url, float) and  math.isnan(url))):
            _url = get_trimmed_url(url)

            result = get_result(esclient, _url, False)
            if result is None:
                not_found_contents.append({"url" : _url})

    return {
        "assets" : not_found_assets,
        "contents" : not_found_contents}

def print_results(results, prefix = '', type = 'csv'):
    df_results = pd.DataFrame(results)
    _file_name = prefix + 'not import'
    _file = ("%soutput/%s.%s") % (root_folder, _file_name, type)

    if type == 'xlsx':
        df_results.to_excel(_file, index=False)
    elif type == 'csv':
        df_results.to_csv(_file, index=False)

def process():
    esclient = get_elastic_client()

    input_assets = [
        "https://www.facebook.com/wawawaku",
        "https://www.instagram.com/xxzyxxzy/",
        "https://www.instagram.com/soya_plan/",
        "https://www.facebook.com/soyalpaca",
        "https://www.instagram.com/t.a.n.aaa/",
        "https://www.facebook.com/febeuuu",
        "https://www.facebook.com/happy0725/",
        "https://www.instagram.com/icwang2015/",
        "https://www.instagram.com/hihi_niniko/"
    ]

    input_contents = [
        "https://www.facebook.com/wawawaku/posts/324801355676134",
        "https://www.instagram.com/p/CPnnHVtge9i/",
        "https://www.instagram.com/p/CQBJD43JkbQ/",
        "https://www.facebook.com/100044604031778/posts/344700590360073/",
        "https://www.instagram.com/p/CQGfZbHrb9Q/",
        "https://www.facebook.com/100044473927807/posts/343789127113549/",
        "https://www.facebook.com/1660874470799073/posts/3103254483227724/",
        "https://www.instagram.com/p/CQQoGAvgnyL/",
        "https://www.instagram.com/p/CQYb505nEPs/"
    ]

    data = check(esclient, input_assets, input_contents)

    if len(data['assets']) > 0:
        print_results(data['assets'], "MISSING ASSETS - ")

    if len(data['contents']) > 0:
        print_results(data['contents'], "MISSING CONTENTS - ")

if __name__ == "__main__":

    index_name = constant.es_content_index['TW']
    root_folder = "../"

    process()