import os,sys
import math
import pandas as pd
sys.path.append('../../')
import main

def get_query(url):
    return "SELECT partner_id_ref, asset_id, partner_name FROM vw_pam WHERE url = '%s'" % (url)

def get_asset(url):
    engine = main.create_engine(conn)
    df_input = pd.read_sql_query(get_query(url), con=engine)
    engine.dispose()

    return df_input

def get_asset_details(asset):
    missing_assets = []
    diff_name = []

    for index,url_ind in enumerate(url_index):
        if url_ind in asset:
            if not (asset[url_ind] == '' or (isinstance(asset[url_ind], float) and  math.isnan(asset[url_ind]))):
                _url = main.get_trimmed_url(asset[url_ind])

                db_asset = get_asset(_url)
                if db_asset.empty:
                    missing_assets.append({"url" : _url})
                elif db_asset.partner_name.to_list()[0] != asset[creator_index]:
                    diff_name.append({
                        "partner_id" : db_asset.partner_id_ref.to_list()[0],
                        "name_at_db" : db_asset.partner_name.to_list()[0],
                        "name_at_excel" : asset[creator_index]
                    })

    return {
        "missing" : missing_assets,
        "diff" : diff_name
    }

def process():
    missing_assets = []
    diff_name = []

    main.init_xls_file()
    pd_input = main.get_input_file(root_folder, file_name, "xlsx")
    pd_input_json = pd_input.to_dict(orient='records')

    for index,row in enumerate(pd_input_json):
        print("Checking: " + str(index+1) + " of " + str(len(pd_input_json)) + ".")
        if row[creator_index] not in except_name:
            asset_details = get_asset_details(row)

            print("Missing Asset: %s" % (len(asset_details["missing"])))
            print("Diff Name: %s" % (len(asset_details["diff"])))
            print()

            missing_assets += asset_details["missing"]
            diff_name += asset_details["diff"]
        else:
            print(row[creator_index])
            print()

    main.print_results(missing_assets, root_folder, "DB/MISSING ASSETS -  %s" % (file_name))
    main.print_results(diff_name, root_folder, "DB/DIFF NAME -  %s" % (file_name))

if __name__ == "__main__":
    conn = main.constant.db_conn["main"]

    file_name = "Publisher mContent Upload document"#'INCA_TEMPLATE_Nano-Micro_Batch1'#
    root_folder = "../../"

    creator_index = "Publisher Name"#"Creator Name"#
    published_date_index = "Latest Published Date"
    updated_at_index = "Latest Updated At"
    url_index = [
        "Instagram URL",
        "Youtube Channel URL",
        "Facebook URL",
        "Twitter URL",
        "Website URL",
        "TikTok URL"
    ]
    except_name = [
        "The Quint",
        "The Quint Hindi",
        "Fit The Quint",
        "Scroll",
        "Femina",
        "Filmfare",
        "Momspresso Hindi"
    ]

    process()