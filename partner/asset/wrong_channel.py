import os,sys
import pandas as pd
sys.path.append('../../')
import main

def get_query(asset_id, channel):
    return "SELECT id, channel FROM asset WHERE id = '%s' AND channel = '%s'" % (asset_id, channel)

def get_asset(asset_id, channel):
    engine = main.create_engine(conn)
    df_input = pd.read_sql_query(get_query(asset_id, channel), con=engine)
    engine.dispose()

    return df_input

def process():
    wrong_channel = []
    wrong_channel_ctr = 0
    start_index = -1

    pd_input = main.get_input_file(root_folder, file_name)
    pd_input_json = pd_input.to_dict(orient='records')

    get_checked = main.get_checked(root_folder, file_name)
    if get_checked is not None and not get_checked.empty:
        start_index = get_checked.last_index.to_list()[0]

    for index,row in enumerate(pd_input_json):
        if start_index > index:
            continue

        print("Checking: " + str(index+1) + " of " + str(len(pd_input_json)) + ".")

        asset = get_asset(row['asset_id'], row['channel'])
        print("Wrong channel: %s" % (asset.empty))
        print()

        if asset.empty:
            wrong_channel.append(row)
            wrong_channel_ctr += 1

        checked = [
            {
                "last_index" : index
            }
        ]
        main.print_results(checked, root_folder, "Checked/%s" % (file_name))

        _file_name = file_name
        if start_index > -1:
            _file_name += " " + str(start_index)
        print("Wrong Channel - Total : %s" % (wrong_channel_ctr))
        main.print_results(wrong_channel, root_folder, _file_name)

if __name__ == "__main__":
    conn = main.constant.db_conn["main"]
    root_folder = "../../"
    file_name = "WRONG_CHANNEL"

    process()