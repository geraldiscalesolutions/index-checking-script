import os,sys
import pandas as pd
from elasticsearch import helpers
sys.path.append('../../')
import main

def get_query(market_id):
    return "SELECT url, channel FROM vw_pam WHERE market_id='%s' AND channel in ('facebook', 'instagram', 'youtube')" % (market_id)

def get_input():
    print("Getting input from database...")
    engine = main.create_engine(conn)
    df_input = pd.read_sql_query(get_query(market_id), con=engine)
    engine.dispose()

    return df_input

def set_asset_handle(row):
    return str(row.url).replace(asset_domain[row.channel], "")

def get_es_query_dv(username):
    return {
        "query": {
            "term" : {
                "username.display_value" : username
            }
        },
        "sort" : {
            "created_at" : "desc"
        }
        ,
        "size" : 1
    }

def get_es_query(username):
    return {
        "query": {
            "term" : {
                "username" : username
            }
        },
        "sort" : {
            "created_at" : "desc"
        }
        ,
        "size" : 1
    }

def get_result(esclient, username, is_retry = False):
    if is_retry:
        qry = get_es_query(username)
    else:
        qry = get_es_query_dv(username)

    result = helpers.scan(client=esclient, query=qry, index=index_name, preserve_order=True)
    df_result = pd.DataFrame(result)

    if not df_result.empty and df_result._source[0] is not None:
        return df_result._source[0]
    elif not is_retry:
        return get_result(esclient, username, True)

    return None

def process():
    esclient = main.get_elastic_client()
    with_data = []
    with_data_ctr = 0
    no_data = []
    no_data_ctr = 0

    df_input = get_input()
    for row in df_input.itertuples():
        print("Checking: " + str(row.Index+1) + " of " + str(df_input.url.count()) + ".")
        username = set_asset_handle(row)
        # df_input['handle'] = df_input.apply(lambda row: set_asset_handle(row), axis = 1)
        result = get_result(esclient, username)

        print("Has Data: %s" % (result is not None))
        print()
        if result is not None:
            with_data.append(result)
            with_data_ctr += 1
        else :
            no_data.append({
                "url" : row.url,
                "channel" : row.channel,
                "handle" : username
            })
            no_data_ctr += 1

    main.print_results(with_data, root_folder, file_name)
    main.print_results(no_data, root_folder, "No Affable Data - %s" % (file_name))

    print("Total With Affable Data : %s" % (with_data_ctr))
    print("Total No Affable Data : %s" % (no_data_ctr))

if __name__ == "__main__":
    market_id = "VN"
    conn = main.constant.db_conn["main"]
    index_name = main.constant.es_content_index["asset_audience"]
    root_folder = "../../"
    file_name = market_id + " Asset Audience"

    asset_domain = {
        "youtube" : "https://www.youtube.com/channel/",
        "facebook" : "https://www.facebook.com/",
        "instagram" : "https://www.instagram.com/"
    }

    process()