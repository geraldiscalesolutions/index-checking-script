import os,sys
sys.path.append('../')
import partner, input

def set_asset_details(asset_details, row_ind, isNone = True):
    if isNone:
        asset_details[row_ind] = None
        asset_details[row_ind + " " + published_date_index] = None
        asset_details[row_ind + " " + updated_at_index] = None
    else:
        asset_details[row_ind + " " + published_date_index] = "NOT FOUND"
        asset_details[row_ind + " " + updated_at_index] = "NOT FOUND"

    return asset_details

def get_asset_details(asset):
    asset_details = {
        creator_index : asset[creator_index],
        "partner_id" : None
    }
    asset_ctr = 0
    diff_name = []
    not_found = {}
    for _ind in not_found_index:
        not_found[_ind] = []

    for index,row_ind in enumerate(row_index):
        if row_ind in asset:
            check_name = asset[creator_index] if index == 0 else None
            asset_info = partner.check_asset(url = asset[row_ind], check_name=check_name)

            if asset_info is None:
                asset_details = set_asset_details(asset_details, row_ind)
            else:
                asset_ctr += 1
                asset_details[row_ind] = asset_info["url"]

                if not asset_info["is_found"]:
                    not_found["all"].append({
                        "url" : asset_info["url"]
                    })
                    asset_details = set_asset_details(asset_details, row_ind, False)

                    if asset_info["not_found_at"] == "db":
                        not_found["db"].append({
                            "url" : asset_info["url"]
                        })
                    else:
                        to_append = {
                            "url" : asset_info["url"],
                            "partner_id" : asset_info["partner_id"],
                            "asset_id" : asset_info["asset_id"],
                        }

                        if "data_status" in asset_info:
                            to_append["data_status"] = asset_info["data_status"]

                        not_found[asset_info["not_found_at"]].append(to_append)

                if asset_info["has_details"]:
                    asset_details["partner_id"] = asset_info["partner_id"]
                    asset_details[row_ind + " asset_id"] = asset_info["asset_id"]

                    if asset_info['is_found']:
                        asset_details[row_ind + " " + published_date_index] = asset_info['published_date']
                        asset_details[row_ind + " " + updated_at_index] = asset_info['updated_at']

                    if "diff_name" in asset_info:
                        diff_name.append({
                            "partner_id" : asset_info["partner_id"],
                            "partner_id_ref" : asset_info["partner_id_ref"],
                            "name_at_excel" : asset[creator_index],
                            "name_at_db" : asset_info["diff_name"]["db"],
                            "name_at_index" : asset_info["diff_name"]["index"]
                        })

    return {
        "checked" : asset_details,
        "ctr" : asset_ctr,
        "not_found" : not_found,
        "diff_name" : diff_name
    }

def get_asset_details_asset_id(asset):
    asset_details = {
        "partner_id" : None
    }
    asset_ctr = 0
    diff_name = []
    not_found = {}
    for _ind in not_found_index:
        not_found[_ind] = []

    for index,row_ind in enumerate(row_index):
        if row_ind in asset:
            asset_info = partner.check_asset(asset_id = asset[row_ind])

            if asset_info is None:
                asset_details = set_asset_details(asset_details, row_ind)
            else:
                asset_ctr += 1
                asset_details[row_ind] = asset_info["asset_id"]

                if not asset_info["is_found"]:
                    not_found["all"].append({
                        "asset_id" : asset_info["asset_id"]
                    })
                    asset_details = set_asset_details(asset_details, row_ind, False)

                    if asset_info["not_found_at"] == "db":
                        not_found["db"].append({
                            "asset_id" : asset_info["asset_id"]
                        })
                    else:
                        not_found[asset_info["not_found_at"]].append({
                            "partner_id" : asset_info["partner_id"],
                            "asset_id" : asset_info["asset_id"],
                        })

                if asset_info["has_details"]:
                    asset_details["partner_id"] = asset_info["partner_id"]
                    asset_details[row_ind + " asset_id"] = asset_info["asset_id"]

                    if asset_info['is_found']:
                        asset_details[row_ind + " " + published_date_index] = asset_info['published_date']
                        asset_details[row_ind + " " + updated_at_index] = asset_info['updated_at']

    return {
        "checked" : asset_details,
        "ctr" : asset_ctr,
        "not_found" : not_found,
        "diff_name" : diff_name
    }

def print_stat(index = "", checked_ctr = 0, not_found = [], isLen = True):
    print("%s Checked: %s" % (index, checked_ctr))

    for _ind in not_found_index:
        if _ind != "all":
            if isLen:
                stat = len(not_found[_ind])
            else:
                stat = not_found[_ind]
            print("%s Missing at %s: %s" % (index, _ind, stat))

def print_results(market, checked, not_found, diff_name):
    _file_name = "[%s] %s" % (market, file_name)
    add_path = "Asset"
    # partner.main.print_results(results=checked, root_folder=root_folder, add_path=("%s/Latest Date" % (add_path)), file_name=_file_name)
    partner.main.print_results(results=not_found["db"], root_folder=root_folder, add_path=("%s/Missing/DB" % (add_path)), file_name=_file_name)
    partner.main.print_results(results=not_found["dc"], root_folder=root_folder, add_path=("%s/Missing/DC" % (add_path)), file_name=_file_name)
    partner.main.print_results(results=not_found["index"], root_folder=root_folder, add_path=("%s/Missing/Index" % (add_path)), file_name=_file_name)
    partner.main.print_results(results=diff_name, root_folder=root_folder, add_path=("%s/Diff Name" % (add_path)), file_name=_file_name)

def process():
    esclient = partner.main.es.get_elastic_client()
    partner.set_esclient(esclient)
    partner.set_conn(conn)

    total_checked = 0
    not_found_ctr = {}
    for _ind in not_found_index:
        if _ind != "all":
            not_found_ctr[_ind] = 0

    partner.main.init_xls_file()
    for market in markets:
        partner.set_market_id(market)
        checked = []
        checked_ctr = 0
        diff_name = []
        not_found = {}
        for _ind in not_found_index:
            not_found[_ind] = []

        if is_mutiple:
            pd_input = partner.main.get_input_file(root_folder, file_name, "xlsx", market)
        else:
            pd_input = partner.main.get_input_file(root_folder, file_name, "xlsx")

        pd_input_json = pd_input.to_dict(orient="records")
        for index,row in enumerate(pd_input_json):
            print("=====================")
            print("Checking [%s]: Row %s of %s." % (market, str(index+1), str(len(pd_input_json)) ))
            if creator_index not in row or row[creator_index] not in except_name:
                asset_details = get_asset_details(row)
                # asset_details = get_asset_details_asset_id(row)

                print()
                print("With Asset: %s" % (asset_details["ctr"] > 0))
                print("Missing Asset: %s" % (len(asset_details["not_found"]["all"])))
                print("Diff Name: %s" % (len(asset_details["diff_name"]) > 0))
                print()

                checked.append(
                    asset_details["checked"]
                )
                checked_ctr += asset_details["ctr"]
                diff_name += asset_details["diff_name"]
                for _ind in not_found_index:
                    not_found[_ind] += asset_details["not_found"][_ind]
            else:
                print(row[creator_index])
                print()

        total_checked += checked_ctr
        for _ind in not_found_index:
            if _ind != "all":
                not_found_ctr[_ind] += len(not_found[_ind])

        print_stat("[%s]" % (market), checked_ctr, not_found)
        print()
        print_results(market, checked, not_found, diff_name)

    print_stat("Total", total_checked, not_found_ctr, False)

if __name__ == "__main__":
    conn = partner.main.constant.db_conn["main"]
    dc_market = partner.main.constant.es_content_index["DC"]
    markets = input.markets
    is_mutiple = input.is_multiple

    file_name = input.file_name
    root_folder = "../../"

    creator_index = input.creator_index
    published_date_index = "Latest Published Date"
    updated_at_index = "Latest Updated At"
    row_index = [
        "Instagram URL",
        "Youtube Channel URL",
        "Facebook URL",
        "Twitter URL",
        "Website URL",
        "TikTok URL",
        "asset_id"
    ]
    except_name = input.except_name

    not_found_index = [
        "db",
        "dc",
        "index",
        "all"
    ]

    process()