import os,sys
import math
import pandas as pd
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from urllib.parse import quote
sys.path.append('../../')
import constant


def create_session(engine):
    session = sessionmaker(bind=engine)
    return session()

def get_create_engine(config):
    return db.create_engine("mysql+pymysql://%s:%s@%s:%s/%s" % (config['username'], quote(config['password']), config['host'], config['port'], config['database']))

def create_conn(config):
    engine = get_create_engine(config)
    return engine.connect()

def get_conn(market_id):
    global conns
    if market_id not in conns:
        conn = create_conn(constant.db_conn[market_id])
        conns[market_id] = conn
    else:
        conn = conns[market_id]

    return conn

def get_query(partner_id):
    return 'SELECT id, url, asset_type FROM asset WHERE partner_id = %s ' % (partner_id)

def get_results(conn, partner_id, market_id):
    qry = get_query(partner_id)
    _res = []

    result_proxy = conn.execute(qry)
    result_set = result_proxy.fetchall()

    if len(result_set) > 0:
        for index, row in enumerate(result_set):
            # if index == 0:
                _res.append({
                    "partner_id" : partner_id,
                    "market_id" : market_id,
                    "type" : row[2],
                    "asset_id" : row[0],
                    "url" : row[1]
                })
            # else:
            #     _res.append({
            #         "partner_id" : None,
            #         "market_id" : None,
            #         "type" : row[2],
            #         "asset_id" : row[0],
            #         "url" : row[1]
            #     })

        return _res


    return None
    

def print_results(results, not_found = False):
    df_results = pd.DataFrame(results)
    _file_name = file_name
    if not_found:
        _file_name = "NO ASSETS FOUND - " + file_name
    
    df_results.to_csv(root_folder + "output/" + _file_name + ".csv", index=False)
    # df_results.to_excel(root_folder + "output/" + _file_name + ".xlsx", index=False)

def process():
    with_assets = []
    no_assets = []

    pd_input = pd.read_csv(root_folder + "input/" + file_name + ".csv")
    pd_input_json = pd_input.to_dict(orient='records')

    for index,row in enumerate(pd_input_json):
        print("Query : " + str(index+1) + " of " + str(len(pd_input_json)) + ".")
        if row[partner_id_index] and row[market_id_index]:
            conn = get_conn(row[market_id_index])
            results = get_results(conn, row[partner_id_index], row[market_id_index])
            print(results)
            print()
            if results is None:
                no_assets.append({
                    "partner_id" : row[partner_id_index],
                    "market_id" : row[market_id_index]
                })
            else:
                with_assets += results

    print_results(with_assets)
    print_results(no_assets, True)

if __name__ == "__main__":

    conns = {}
    file_name = 'roque_partners'
    root_folder = "../../"

    id_index = 'id'
    partner_id_index = 'partner_id'
    market_id_index = 'market_id'

    process()