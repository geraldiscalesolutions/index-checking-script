import os,sys
import pandas as pd
from elasticsearch import helpers
sys.path.append('../../')
import main

def get_query(asset_id):
    return {
        "query": {
            "term" : {
                "asset_id.display_value" : asset_id
            }
        },
        "_source" : [
            "channel"
        ],
        "size" : 1
    }

def get_result(esclient, index_name, asset_id):
    qry = get_query(asset_id)
    result = helpers.scan(client=esclient, query=qry, index=index_name, preserve_order=True)
    list_result = list(result)
    df_result = pd.DataFrame(list_result)

    if not df_result.empty and list_result[0]['_source']:
        return list_result[0]['_source']

    return None

def process():
    esclient = main.get_elastic_client()

    for market in markets:
        index_name = main.constant.es_content_index[market]
        output_file = "[%s] %s" % (market, file_name)
        wrong_channel = []
        wrong_channel_ctr = 0
        missing_asset = []
        missing_asset_ctr = 0
        start_index = -1

        pd_input = main.get_input_file(root_folder, file_name)
        pd_input_json = pd_input.to_dict(orient='records')

        get_checked = main.get_checked(root_folder, output_file)
        if get_checked is not None and not get_checked.empty:
            start_index = get_checked.last_index.to_list()[0]
            wrong_channel_ctr = get_checked.wrong_channel.to_list()[0]
            missing_asset_ctr = get_checked.missing_asset.to_list()[0]

        for index,row in enumerate(pd_input_json):
            status = "Nice"

            if start_index > index:
                continue

            print("Checking: " + str(index+1) + " of " + str(len(pd_input_json)) + ".")

            asset = get_result(esclient, index_name, row['asset_id'])
            if asset is None:
                missing_asset.append(row)
                missing_asset_ctr += 1
                status = "Missing"
            elif asset is not None and asset["channel"] != row["channel"]:
                wrong_channel.append(row)
                wrong_channel_ctr += 1
                status = "Wrong"

            print("Status: %s" % (status))
            print()

            checked = [
                {
                    "last_index" : index,
                    "wrong_channel" : wrong_channel_ctr,
                    "missing_asset" : missing_asset_ctr,
                }
            ]
            main.print_results(checked, root_folder, "Checked/%s" % (output_file))

            print("Wrong Channel - Total : %s" % (wrong_channel_ctr))
            print("Missing Asset - Total : %s" % (missing_asset_ctr))
            print()

            _file_name = output_file
            if start_index > -1:
                _file_name += " " + str(start_index)
            main.print_results(wrong_channel, root_folder, _file_name)
            main.print_results(missing_asset, root_folder, "MISSING ASSET - %s" % (_file_name))

if __name__ == "__main__":
    markets = [
        "content"
    ]
    root_folder = "../../"
    file_name = "WRONG_CHANNEL"

    process()