import math
import sys
sys.path.append('../')
sys.path.append('../../')
import main

def get_asset_query(market_id, url = None, asset_id = None):
    if market_id == "IN2":
        market_id = "I2"

    _select = "SELECT asset_id, asset_id_ref, partner_id, partner_id_ref, partner_name "
    _from = "FROM vw_pam "
    _where = "WHERE market_id = '%s' " % (market_id)

    if url is not None:
        _where += "AND url regexp '%s'" % url
    elif asset_id is not None:
        _where += "AND asset_id = '%s'" % (asset_id)

    return _select + _from + _where

def get_es_latest_date_query(type = "published_date", content_id = None, content_url = None, url = None, asset_id = None):
    term = ""
    source = [
        type,
        "partner.id",
        "partner.name",
        "data_status"
    ]

    if content_id is not None:
        term = {
            "id.display_value": content_id
        }
    elif content_url is not None:
        term = {
            "url.display_value": content_url
        }
    elif url is not None:
        term = {
            "asset.url.display_value": url
        }
    elif asset_id is not None:
        term = {
            "asset_id.display_value": asset_id
        }

    return {
        "query": {
            "term": term
        },
        "sort" : {
            type : "desc"
        },
        "_source" : source,
        "size" : 1
    }

def check_asset(url = None, asset_id = None, check_name = None):
    esclient = get_esclient()
    conn = get_conn()
    market_id = get_market_id()
    index_name = get_index_name()
    dc_index_name = get_dc_index_name()
    dah_index_name = get_dah_index_name()

    to_check = url if url is not None else asset_id
    asset_info = None
    if main.is_empty(to_check):
        return asset_info
    else:
        asset_info = {
            "is_found" : True,
            "has_details" : True
        }
        if url is not None:
            to_check = main.get_trimmed_url(url)
            asset_info["url"] = to_check
        elif asset_id is not None:
            to_check = main.get_trimmed(asset_id)

        print()
        print("Asset: %s" % (to_check))

        if url is not None:
            df_input = main.db.get_db_data(conn, get_asset_query(market_id, url=to_check))
        elif asset_id is not None:
            df_input = main.db.get_db_data(conn, get_asset_query(market_id, asset_id=to_check))

        if df_input.empty:
            asset_info["is_found"] = False
            asset_info["has_details"] = False
            asset_info["not_found_at"] = "db"
        else:
            asset_id = df_input.asset_id.to_list()[0]
            asset_id_ref = df_input.asset_id_ref.to_list()[0]
            asset_info["asset_id"] = asset_id
            partner_id = df_input.partner_id.to_list()[0]
            asset_info["partner_id"] = partner_id
            partner_id_ref = df_input.partner_id_ref.to_list()[0]
            asset_info["partner_id_ref"] = partner_id_ref

            _source = main.es.get_es_data(esclient, dc_index_name, get_es_latest_date_query(asset_id=asset_id))
            print("Has DC data: %s" % (_source is not None))
            if _source is None:
                asset_info["is_found"] = False
                asset_info["not_found_at"] = "dc"

                dah_es_data = main.es.get_es_data(esclient, dah_index_name, get_es_latest_date_query(type="created_at", asset_id=asset_id))
                print("Has data_asset_history data: %s" % (dah_es_data is not None))
                asset_info["data_status"] = None if dah_es_data is None else dah_es_data["data_status"]
            else:
                if url is not None:
                    _source_pub_date = main.es.get_es_data(esclient, index_name, get_es_latest_date_query(url=to_check))
                elif asset_id is not None:
                    _source_pub_date = main.es.get_es_data(esclient, index_name, get_es_latest_date_query(asset_id=asset_id_ref))

                if _source_pub_date is None:
                    asset_info["is_found"] = False
                    asset_info["not_found_at"] = "index"
                else:
                    if url is not None and check_name is not None:
                        if check_name.casefold() != _source_pub_date['partner']['name'].casefold():
                            partner_name = df_input.partner_name.to_list()[0]
                            asset_info["diff_name"] = {
                                "db" : partner_name,
                                "index" : _source_pub_date['partner']['name']
                            }

                    asset_info["published_date"] = _source_pub_date['published_date']

                    if url is not None:
                        _source_updated_at = main.es.get_es_data(esclient, index_name, get_es_latest_date_query(type="updated_at", url=to_check))
                    elif asset_id is not None:
                        _source_updated_at = main.es.get_es_data(esclient, index_name, get_es_latest_date_query(type="updated_at", asset_id=asset_id_ref))

                    if _source_updated_at is None:
                        asset_info["updated_at"] = "NOT FOUND"
                    else:
                        asset_info["updated_at"] = _source_updated_at['updated_at']

                print("Has index data: %s" % (_source_pub_date is not None))

    return asset_info

def print_asset_results(root_folder, market, file_name, not_found = None, diff_name = None):
    _file_name = "[%s] %s" % (market, file_name)
    asset_add_path = "Asset"
    if not_found is not None:
        main.print_results(results=not_found["db"], root_folder=root_folder, add_path=("%s/Missing/DB" % (asset_add_path)), file_name=_file_name)
        main.print_results(results=not_found["dc"], root_folder=root_folder, add_path=("%s/Missing/DC" % (asset_add_path)), file_name=_file_name)
        main.print_results(results=not_found["index"], root_folder=root_folder, add_path=("%s/Missing/Index" % (asset_add_path)), file_name=_file_name)

    if diff_name is not None:
        main.print_results(results=diff_name, root_folder=root_folder, add_path=("%s/Diff Name" % (asset_add_path)), file_name=_file_name)

def check_content(url):
    esclient = get_esclient()
    index_name = get_index_name()
    dc_index_name = get_dc_index_name()

    if not main.is_empty(url):
        url = main.get_trimmed(url)

        print()
        print("Content: %s" % (url))

        content_id = main.SocialAsset.generate_url_id(url)
        qry = get_es_latest_date_query(content_id = content_id)

        result = main.es.get_es_data(esclient, dc_index_name, qry)
        print("Has DC data: %s" % (result is not None))
        if result is None:
            return {
                "at" : "dc",
                "url" : url,
                "id" : content_id
            }
        else:
            qry = get_es_latest_date_query(content_url = url)
            result = main.es.get_es_data(esclient, index_name, qry)
            print("Has Index data: %s" % (result is not None))
            if result is None:
                return {
                    "at" : "index",
                    "url" : url,
                    "id" : content_id
                }

    return None

def print_content_results(root_folder, market, file_name, not_found = None):
    _file_name = "[%s] %s" % (market, file_name)
    asset_add_path = "Content"
    if not_found is not None:
        main.print_results(results=not_found["dc"], root_folder=root_folder, add_path=("%s/Missing/DC" % (asset_add_path)), file_name=_file_name)
        main.print_results(results=not_found["index"], root_folder=root_folder, add_path=("%s/Missing/Index" % (asset_add_path)), file_name=_file_name)

def set_conn(_conn):
    global glob_conn
    glob_conn = _conn

def get_conn():
    global glob_conn

    return glob_conn

def set_esclient(_esclient):
    global glob_esclient
    glob_esclient = _esclient

def get_esclient():
    global glob_esclient

    return glob_esclient

def set_market_id(_market_id):
    global glob_market_id
    glob_market_id = _market_id
    set_index_name(_market_id)

def get_market_id():
    global glob_market_id

    return glob_market_id

def set_index_name(_market_id):
    global glob_index_name
    glob_index_name = main.constant.es_content_index[_market_id]

def get_index_name():
    global glob_index_name

    return glob_index_name

def get_dc_index_name():
    return main.constant.es_content_index["DC"]

def get_dah_index_name():
    return main.constant.es_content_index["DAH"]