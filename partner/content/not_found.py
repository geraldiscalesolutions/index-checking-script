import sys
sys.path.append('../')
import partner

def process():
    esclient = partner.main.es.get_elastic_client()
    partner.set_esclient(esclient)
    partner.set_market_id(market_id)

    not_found = {
        "dc" : [],
        "index" : []
    }
    add_path = "Content/Missing/"

    pd_input = partner.main.get_input_file(root_folder, file_name)
    pd_input_json = pd_input.to_dict(orient='records')

    for index,row in enumerate(pd_input_json):
        print("Checking: " + str(index+1) + " of " + str(len(pd_input_json)) + ".")
        if "stories" not in row['url']: #and "story" not in row['url']:
            content = partner.check_content(row['url'])

            print("Missing Content: %s" % (content is not None))
            print()

            if content is not None:
                not_found_at = content["at"]
                content.pop("at")
                not_found[not_found_at].append(content)
        else:
            print("Story : %s" % (row['url']))
            print()

    print('Total Missing Content [dc]: %s' % (len(not_found["dc"])))
    print('Total Missing Content [index]: %s' % (len(not_found["index"])))

    partner.print_content_results(root_folder = root_folder, market = market_id, file_name = file_name, not_found=not_found)

if __name__ == "__main__":
    market_id = "PH"
    file_name = 'Bioflu W1 Tracker'
    root_folder = "../../"

    process()