from os import path
import sys
import math
from pathlib import Path
import pandas as pd
from urllib.parse import urlparse
import constant, db_local as db, es_local as es
from utilities.incatech.utility.SocialAsset import SocialAsset

def get_input_file(root_folder = "", file_name = "", type = "csv", sheet_name = None):
    input_file = "%sinput/%s.%s" % (root_folder, file_name, type)

    if type == "csv":
        return pd.read_csv(input_file)
    elif type == "xlsx":
        global xls_file
        if xls_file is None:
            xls_file = pd.ExcelFile(input_file)
        input_file = xls_file

        if sheet_name is None:
            return pd.read_excel(input_file)
        else:
            return pd.read_excel(input_file, sheet_name = sheet_name)

def get_checked(root_folder = "", file_name = "", type = "csv"):
    input_file = "%soutput/Checked/%s.%s" % (root_folder, file_name, type)

    if path.exists(input_file):
        if type == "csv":
            return pd.read_csv(input_file)
        elif type == "xlsx":
            global xls_file
            if xls_file is None:
                xls_file = pd.ExcelFile(input_file)
            input_file = xls_file

            return pd.read_excel(input_file)

    return None

def init_xls_file():
    global xls_file
    xls_file = None

def print_results(results, root_folder = "", add_path="a", file_name = "", type = "csv"):
    if len(results) > 0:
        _path = ("%soutput/%s") % (root_folder, add_path)
        Path(_path).mkdir(parents=True, exist_ok=True)

        df_results = pd.DataFrame(results)
        _file = ("%s/%s.%s") % (_path, file_name, type)

        print("Exporting: %s > %s.%s" % (add_path, file_name, type))
        print()

        if type == "csv":
            df_results.to_csv(_file, index=False)
        elif type == "xlsx":
            df_results.to_excel(_file, index=False)

def is_empty(_str):
    return (_str == '' or (isinstance(_str, str) and _str.strip()) == '-' or (isinstance(_str, float) and  math.isnan(_str)))

def get_trimmed(_str):
    _str = _str.strip()
    _str = _str.strip('/')
    _str = _str.strip(' ')
    _str = _str.strip('/')
    _str = _str.strip(' ')

    return _str

def get_trimmed_url(url, is_asset = True):
    url = url.strip().strip('/')

    if is_asset:
        _dom = [
            "youtube",
            "facebook",
            "twitter",
            "instagram",
            "tiktok"
        ]

        urlparsed = urlparse(url)
        for dom in _dom:
            if dom in urlparsed.netloc:
                url = urlparsed.scheme + "://" + urlparsed.netloc + str(urlparsed.path).replace("//", "/")
                _url = SocialAsset.clean_url(url, dom)
                if _url is not None:
                    url = _url

                break

    return url