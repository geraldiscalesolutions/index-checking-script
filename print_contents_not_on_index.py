import os,sys
import json
import pandas as pd
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from elasticsearch import Elasticsearch, helpers

import constant


def create_session(engine):
    session = sessionmaker(bind=engine)
    return session()

def create_engine(config):
    return db.create_engine("mysql+pymysql://"+config["user"]+":"+config["password"]+"@"+config["host"]+":"+str(config["port"])+"/"+config["database"])

def get_elastic_client():
    es_client_servers = Elasticsearch(
        constant.es_server,
        http_compress=True,
        timeout=60)

    return es_client_servers

def get_result(url, esclient):
    qry = {
        "query": {
            "term":{
                "url": url
            }
        },
        "_source": ["url"],
        "size": 1
    }

    return helpers.scan(client=esclient, query=qry, index=index_name, doc_type="_doc", size=10000, scroll='60m', raise_on_error=False)

def print_results(results):
    df_results = pd.DataFrame(results)
    # df_results.to_csv("output/" + file_name + ".csv", index=False)
    df_results.to_excel("output/" + file_name + ".xlsx", index=False)

def process():
    esclient = get_elastic_client()
    contents_checked = [];

    pd_input = pd.read_csv('input/' + file_name + '.csv')
    for index, url in pd_input.iterrows():
        result = get_result(url['url'], esclient)
        df_result = pd.DataFrame(result);
        if df_result.empty:
            contents_checked.append(
                {"url": url['url']}
            )
    
    print_results(contents_checked)

if __name__ == "__main__":

    index_name = constant.es_content_index['PH']
    file_name = 'Bioflu W1 Tracker'

    process()