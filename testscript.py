import os,sys
import json
import pandas as pd
import sqlalchemy as db
from sqlalchemy.orm import sessionmaker
from elasticsearch import Elasticsearch, helpers


def create_session(engine):
    session = sessionmaker(bind=engine)
    return session()

def create_engine(config):
    return db.create_engine("mysql+pymysql://"+config["user"]+":"+config["password"]+"@"+config["host"]+":"+str(config["port"])+"/"+config["database"])

def get_elastic_client():
    es_client_servers = Elasticsearch(
        es_servers,
        http_compress=True,
        timeout=60)

    return es_client_servers

def process():
    url_str = [
        'https://www.instagram.com/p/CPQNcaCJDBM',
        'https://www.instagram.com/p/CPQAtWeMg9l',
        'https://www.instagram.com/p/CPPx3gOrUG4',
        'https://www.instagram.com/p/CPPtu0iJue',
        'https://www.instagram.com/p/CPR7iCvHjVs',
        'https://www.instagram.com/p/CPX4XEFBA23',
        'https://www.instagram.com/p/CPXZ4XAp24S',
        'https://www.instagram.com/p/CPX1FTXHVRF',
        'https://www.instagram.com/p/CPXjJY0hrof',
        'https://www.instagram.com/p/CPaXn2YjTo_',
        'https://www.instagram.com/p/CPZqD0LjKmj',
        'https://www.instagram.com/p/CPabWzxLmwQ',
        'https://www.instagram.com/p/CPaJB4Yll8A',
        'https://www.instagram.com/p/CPZ1MwInGLL',
        'https://www.instagram.com/p/CPZ2J-FjP4Z']

    data_df = []
    esclient = get_elastic_client()
    for i in url_str:
        qry = {
            "query": {
                "term":{
                    "url": i
                }
            },
            "_source": ["url"]
        }

        results = helpers.scan(client=esclient,
                               query=qry,
                               index=index_name,
                               doc_type="_doc",
                               size=10000,
                               scroll='60m',
                               raise_on_error=False
                               )
        flag = True
        for x in results:
            data_df.append(
                {"url": i, "status": "existing"}
            )
            flag = False

        if flag:
            data_df.append(
                {"url": i, "status": "not existing"}
            )

    df = pd.DataFrame(data_df)
    # df.to_csv("/Users/sonny.cabali/Desktop/test.csv", index=False)
    print(df)

    url_list = df.url.to_list()
    print(url_list)

if __name__ == "__main__":

    es_servers = [
        "localhost:9201",
        "localhost:9202",
        "localhost:9203",
        "localhost:9204"
    ]

    index_name = "data_content_ph"

    process()