es_server = [
    "localhost:9201",
    "localhost:9202",
    "localhost:9203",
    "localhost:9204"
]

es_content_index = {
    "PH" : "data_content_ph_v2.1",
    "ID" : "data_content_id_v2.1",
    "IN" : "data_content_in_v2.1",
    "IN2" : "data_content_in2_v2.1",
    "MY" : "data_content_my_v2.1",
    "TW" : "data_content_tw_v2.1",
    "NZ" : "data_content_nz_v2.1",
    "SG" : "data_content_sg_v2.1",
    "UK" : "data_content_uk_v2.1",
    "VN" : "data_content_vn_v2.1",
    "HK" : "data_content_hk_v2.1",
    "KR" : "data_content_kr_v2.1",
    "DC" : "data_content",
    "DAH" : "data_asset_history",
    "asset_audience" : "data_asset_audience"
}

db_conn = {
    "main" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 4306,
        "database" : "incatech_data"
    },
    "HK" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3903,
        "database" : "incatech_data_hk"
    },
    "ID" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3903,
        "database" : "incatech_data_id"
    },
    "MY" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3903,
        "database" : "incatech_data_my"
    },
    "TW" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3903,
        "database" : "incatech_data_tw"
    },
    "IN" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3901,
        "database" : "incatech_data_in"
    },
    "IN2" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3901,
        "database" : "mcontent_data_in"
    },
    "PH" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3902,
        "database" : "incatech_data_ph"
    },
    "NZ" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3902,
        "database" : "incatech_data_nz"
    },
    "SG" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3902,
        "database" : "incatech_data_sg"
    },
    "TH" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3902,
        "database" : "incatech_data_th"
    },
    "UK" : {
        "host" : "127.0.0.1",
        "username" : "",
        "password" : "",
        "port" : 3901,
        "database" : "incatech_data_uk"
    }
}